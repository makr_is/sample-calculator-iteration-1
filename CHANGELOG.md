# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]()

## [0.1.0]() - 2020-06-02
> WARNING: initital prototype which is not ready for productive use! in section 0.1.0

### Added

- Prototype calculates average, variance, and deviation.
- Sample values can be provided manually or by a file in CSV format.
